require('dotenv').config();
const Promise = require('bluebird');
const { Op } = require('sequelize');
const { POConn, PRConn, setPOTransaction, setPRTransaction, connectAll } = require('./db_bootstrap');
const { call } = require('./lambda/invoker');

const PurchaseOrderRepo = require('./repositories/purchase_order_repository');
const PurchaseOrderItemRepo = require('./repositories/purchase_order_item_repository');
const SalesOrderRepo = require('./repositories/sales_order_repository');
const SalesOrderItemRepo = require('./repositories/sales_order_item_repository');
const QuotationRepo = require('./repositories/quotation_repository');
const QuotationItemRepo = require('./repositories/quotation_item_repository');
const PurchaseRequestSnapshotRepo = require('./repositories/purchase_request_snapshot_repository');
const PurchaseRequestRepo = require('./repositories/purchase_request_repository');

const SalesOrderModel = require('./models/purchase-order/sales_order');
const SalesOrderItemModel = require('./models/purchase-order/sales_order_item');
const PurchaseOrderModel = require('./models/purchase-order/purchase_order');
const PurchaseOrderItemModel = require('./models/purchase-order/purchase_order_item');
const QuotationModel = require('./models/purchase-request/quotation');
const QuotationItemModel = require('./models/purchase-request/quotation_item');
const PurchaseRequestModel = require('./models/purchase-request/purchase_request');

const migrate = async () => {
    let IDS = [];

    // Get SO data per chunk to process
    const SOData = await SalesOrderRepo.findAllNoLimit({
        channel_type: 'QR'
    });

    // Loop SO Data per page
    await Promise.map(SOData, async (SOCustomerOld) => {
        // Save cahnged ids for later use
        const SODataForS2s = {
            sales_order_no: SOCustomerOld.sales_order_no,
            purchase_request_no: SOCustomerOld.purchase_request_no,
            purchase_request_id: SOCustomerOld.purchase_request_id,
            sales_order_no: SOCustomerOld.sales_order_no,
            PODatas: []
        };

        // IDS.push();
        console.log(`--- Starting for BCI Customer to Vendor Flow SO ID ${SOCustomerOld.id} ---`);
        // Start Process per SO

        //-------------------------------------------- Create new Customer to BCI Vendor Flow
        // Create new SO Customer
        const SOCustomerNew = SalesOrderModel.createSOCustomerFromSOCustomerOld(SOCustomerOld);


        await SalesOrderRepo.updateToMP(SOCustomerOld.id)
        console.log(`Update SO Parent Information to MP and Channel 4`);

        // GET all current PO BCI from by current SO number
        const POsBCIOld = await PurchaseOrderRepo.findAllNoLimit({
            sales_order_no: SOCustomerOld.sales_order_no
        });

        // Loop each PO from current SO
        let index = 1;
        await Promise.map(POsBCIOld, async (POBCIOld) => {
            const originalSellerInformation = {
                po_no: POBCIOld.purchase_order_no,
                company_seller_id: POBCIOld.company_seller_id,
                company_seller_name: POBCIOld.company_seller_name
            };

            SODataForS2s.PODatas.push(originalSellerInformation);

            // Get PO Items BCI Old for reference
            const POItemsBCIOld = await PurchaseOrderItemRepo.findAllNoLimit({
                purchase_order_id: POBCIOld.id
            });

            // Create new PO BCI
            console.log(`Create new PO BCI`);
            const POBCICustomerNew = await PurchaseOrderRepo.create(PurchaseOrderModel.createPOBCICustomerFromPOBCICustomerOld(POBCIOld, SOCustomerNew));

            // Create new PO Items BCI
            console.log(`Create new PO Items BCI`);
            await PurchaseOrderItemRepo.createMany(PurchaseOrderItemModel.createPOItemBCIFromPOItemBCIOld(POBCICustomerNew, POItemsBCIOld));

            //-------------------------------------------- Create BCI Customer to Vendor Flow
            console.log(`# Starting for BCI Customer to Vendor Flow`);
            // Create new Quotation for BCI as Customer
            const QuotationBCI = await QuotationRepo.create(QuotationModel.createQuotationFromPOBCI(POBCICustomerNew, index));
            console.log(`Create Quotation BCI`);

            const QuotationItemsSnapshotBCI = await QuotationItemModel.createQuotationItemSnapshotBCIFromPOBCI(
                QuotationBCI,
                POBCIOld,
                POItemsBCIOld,
                SOCustomerNew,
                originalSellerInformation.company_seller_id,
                originalSellerInformation.company_seller_name
            );
            console.log(`Create Quotation Item Snapshot BCI`);

            await Promise.map(QuotationItemsSnapshotBCI, async (qn) => {
                const result = await PurchaseRequestSnapshotRepo.createQuotationItemSnapshot(qn);
                qn.shopping_cart_item_id = result;
            });

            // Create Quotation Items
            await Promise.map(QuotationItemsSnapshotBCI, async (qnItem) => {
                await QuotationItemRepo.create(qnItem);
            });
            console.log(`Create Quotation Item BCI`);

            // Create BCI Customer PR
            const PRBCICustomer = await PurchaseRequestRepo.create(PurchaseRequestModel.createPRBCICustomerFromPOBCI(QuotationBCI, POBCIOld, POItemsBCIOld));
            console.log(`Create PR BCI`);

            // Create BCI Customer SO
            const SOBCINew = await SalesOrderRepo.create(SalesOrderModel.createSOBCIFromPOBCI(PRBCICustomer, POBCIOld, POItemsBCIOld));
            console.log(`Create SO BCI`);

            // Create BCI Customer SO Items
            await SalesOrderItemRepo.createMany(SalesOrderItemModel.createSOItemBCIFromPOItemBCI(SOBCINew, POItemsBCIOld));
            console.log(`Create SO Item BCI`);

            // Create PO Vendor
            const POVendor = await PurchaseOrderRepo.create(PurchaseOrderModel.createPOVendorFromPOBCICustomerOld(POBCIOld, SOBCINew));
            console.log(`Create PO Vendor`);

            // Create PO Item Vendor
            await PurchaseOrderItemRepo.createMany(PurchaseOrderItemModel.createPOItemVendorFromPOBCIItems(POVendor, POItemsBCIOld));
            console.log(`Create PO Item Vendor`);
        }, {
                concurrency: 1
            });

        IDS.push(SODataForS2s);
        // Deleting old PO's
        await Promise.map(POsBCIOld, async (POBCIOld) => {
            await PurchaseOrderRepo.delete(POBCIOld.id);
            console.log(`Deleting OLD PO BCI ID ${POBCIOld.id}`);

            await PurchaseOrderItemRepo.deleteByPurchaseOrderId(POBCIOld.id);
            console.log(`Deleting OLD PO Items BCI PO ID ${POBCIOld.id}`);
        }, {
                concurrency: 1
            });
    }, {
            concurrency: 1
        });

    return IDS;
};

const shootBCITools = async (SalesOrders) => {
    await Promise.map(SalesOrders, async (SalesOrder) => {
        const payload = await getBCIPayload(SalesOrder);

        if (SalesOrder.sales_order_no === 'SO-1537787367231') {
            console.log(JSON.stringify(payload));
        }
        // await call(
        //     process.env.BCI_TOOLS_LAMBDA_NAME,
        //     process.env.BCI_TOOLS_ACTION_NAME,
        //     payload
        // );
    }, {
            concurrency: 1
        });
};

const getBCIPayload = async (SalesOrderInformation) => {
    const BCIC = {};

    const SalesOrderBCIV = await SalesOrderRepo.findOne({
        sales_order_no: SalesOrderInformation.sales_order_no
    });
    const PurchaseRequestBCIV = await PurchaseRequestRepo.findOne({
        purchase_request_no: SalesOrderInformation.purchase_request_no
    });
    const QuotationItemsBCIV = await QuotationItemRepo.findAllNoLimit({
        quotation_id: PurchaseRequestBCIV.quotation_id
    });
    const PurchaseOrdersBCIV = await PurchaseOrderRepo.findAllNoLimit({
        sales_order_no: SalesOrderInformation.sales_order_no
    });

    // console.log(PurchaseOrdersBCIV);
    await Promise.map(PurchaseOrdersBCIV, async (PurchaseOrderBCIV) => {
        const data = SalesOrderInformation.PODatas.filter((poData) => PurchaseOrderBCIV.purchase_order_no == poData.po_no);

        // console.log(data);
        PurchaseOrderBCIV.company_seller_id = data[0].company_seller_id;
        PurchaseOrderBCIV.company_seller_name = data[0].company_seller_name;

        PurchaseOrderBCIV.items = await PurchaseOrderItemRepo.findAllNoLimit({
            purchase_order_id: PurchaseOrderBCIV.id
        });

        BCIC.PO = await PurchaseOrderRepo.findOne({
            purchase_order_no_internal: PurchaseOrderBCIV.purchase_order_no
        });

        BCIC.SO = await SalesOrderRepo.findOne({
            sales_order_no: BCIC.PO.sales_order_no
        });

        BCIC.PR = await PurchaseRequestRepo.findOne({
            purchase_request_no: BCIC.SO.purchase_request_no
        });
    }, {
            concurrency: 1
        });

    return {
        BCIV: {
            PR: PurchaseRequestBCIV,
            SO: SalesOrderBCIV,
            PO: PurchaseOrdersBCIV,
            PR_Snapshot: await PurchaseRequestSnapshotRepo.findOnePurchaseRequestSnapshot('PR-1540541515349')
        },
        BCIC
    };
};

// Saga Pattern
const start = async () => {
    POt = await POConn.transaction();
    PRt = await PRConn.transaction();

    setPOTransaction(POt);
    setPRTransaction(PRt);

    try {
        await connectAll();
        const SODatas = await migrate();

        await POt.commit();
        console.log('PO Transaction Commited');

        await PRt.commit();
        console.log('PR Transaction Commited');

        // Populating data for BCI Tools purpose
        // await shootBCITools(SODatas);
        // Comment until method is ready

        process.exit();
    } catch (err) {
        console.error(err);

        await POt.rollback();
        await PRt.rollback();

        process.exit();
    }
};

start();