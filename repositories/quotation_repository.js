const { PRTables, PRTransaction } = require('../db_bootstrap');
const { getOffset } = require('../helpers/pagination_helpers');

exports.count = async (where) => {
    return PRTables.Quotation.count({
        where
    });
};

exports.findOne = async (where) => {
    return PRTables.Quotation.findOne({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAll = async (where, limit = 20, page = 1) => {
    return PRTables.Quotation.findAll({
        where,
        offset: getOffset(page, limit),
        limit
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAllNoLimit = async (where) => {
    return PRTables.Quotation.findAll({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.create = async (Quotation) => {
    return PRTables.Quotation.create(Quotation, {
        transaction: PRTransaction
    }).then(result => JSON.parse(JSON.stringify(result)));
};

module.exports = exports;