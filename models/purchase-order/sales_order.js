const { generateSONumber } = require('../../helpers/number_generator');
const { getSOStatusFromPO } = require('../../helpers/status_mapper');

exports.model = (Sequelize, connection) => {
    const SalesOrder = connection.define('SalesOrder', {
        id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.BIGINT,
            title: "",
            formType: "TEXT",
            autoIncrement: true
        },
        purchase_request_id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.BIGINT,
            title: "",
            formType: "TEXT"
        },
        purchase_request_no: {
            allowNull: true,
            defaultValue: '',
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        sales_order_no: {
            allowNull: true,
            defaultValue: '',
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        status: {
            allowNull: false,
            type: Sequelize.INTEGER(10).UNSIGNED,
            title: "50 = Baru, 60 = Siap Dikirim, 70 = Dalam Pengiriman, 75 = Pengiriman Berhasil, 80 = Terkirim, 81 = Ditolak by WMS, 90 = Tidak Disetujui, 100 = Waktu Habis, 110 = Dibatalkan, 120 = Penagihan, 130 = Telah Dibayar",
            formType: "TEXT"
        },
        group_id: {
            allowNull: true,
            defaultValue: '',
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        ref: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        channel: {
            allowNull: false,
            type: Sequelize.INTEGER(1),
            title: "1=QR Online, 2=QR Ecart, 3=QR Punchout, 4= MP Online",
            formType: "TEXT"
        },
        channel_type: {
            allowNull: true,
            defaultValue: 'QR',
            type: Sequelize.ENUM('MP', 'QR'),
            title: "MP=Marketplace, QR = Quasi Retail",
            formType: "TEXT"
        },
        payment_method_id: {
            allowNull: true,
            type: Sequelize.INTEGER(5),
            title: "",
            formType: "TEXT"
        },
        payment_type_id: {
            allowNull: true,
            type: Sequelize.ENUM('CBD', 'TOP'),
            title: "",
            formType: "TEXT"
        },
        company_buyer_id: {
            allowNull: false,
            type: Sequelize.BIGINT,
            title: "",
            formType: "TEXT"
        },
        company_buyer_name: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        is_partial: {
            allowNull: false,
            defaultValue: '0',
            type: Sequelize.INTEGER(1),
            title: "",
            formType: "TEXT"
        },
        notes: {
            allowNull: true,
            type: Sequelize.TEXT,
            title: "",
            formType: "TEXT"
        },
        discount: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "nominal",
            formType: "TEXT"
        },
        shipping_cost: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "",
            formType: "TEXT"
        },
        tax: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "nominal",
            formType: "TEXT"
        },
        subtotal: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "",
            formType: "TEXT"
        },
        so_amount_total: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "",
            formType: "TEXT"
        },
        so_total_sku: {
            allowNull: true,
            type: Sequelize.INTEGER(10),
            title: "",
            formType: "TEXT"
        },
        term_of_payment: {
            allowNull: true,
            type: Sequelize.INTEGER(11),
            title: "",
            formType: "TEXT"
        },
        hub_id: {
            allowNull: true,
            type: Sequelize.INTEGER(11),
            title: "Empty if MP (Bizzy punya warehouse)",
            formType: "TEXT"
        },
        shipping_method: {
            allowNull: true,
            defaultValue: '0',
            type: Sequelize.INTEGER(11).UNSIGNED,
            title: "",
            formType: "TEXT"
        },
        shipping_provider: {
            allowNull: false,
            defaultValue: '',
            type: Sequelize.STRING(50),
            title: "",
            formType: "TEXT"
        },
        shipping_address_id: {
            allowNull: false,
            type: Sequelize.INTEGER(11),
            title: "",
            formType: "TEXT"
        },
        shipping_country: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipping_province: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipping_city: {
            allowNull: true,
            defaultValue: '',
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipping_zipcode: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipping_address: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipping_phone: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipping_phone_prefix: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipping_district: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipping_receiver_name: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipping_receiver_email: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        billing_address_id: {
            allowNull: false,
            type: Sequelize.INTEGER(11),
            title: "",
            formType: "TEXT"
        },
        billing_country: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        billing_province: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        billing_city: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        billing_zipcode: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        billing_address: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        billing_phone: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        billing_phone_prefix: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        billing_district: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        billing_receiver_name: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        billing_receiver_email: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        delivery_notes: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        created_by: {
            allowNull: false,
            type: Sequelize.BIGINT,
            title: "",
            formType: "TEXT"
        },
        creator_type: {
            allowNull: false,
            defaultValue: 'PERSON',
            type: Sequelize.ENUM('PERSON', 'EMPLOYEE'),
            title: "",
            formType: "TEXT"
        },
        expired_at: {
            allowNull: true,
            type: Sequelize.DATE,
            title: "",
            formType: "DATE"
        },
        created_at: {
            allowNull: true,
            type: Sequelize.DATE,
            title: "",
            formType: "DATE"
        },
        updated_at: {
            allowNull: true,
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            type: Sequelize.DATE,
            title: "",
            formType: "DATE"
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
            title: "",
            formType: "DATE"
        }
    }, {
            tableName: 'sales_order',
            freezeTableName: true,
            underscored: true,
            paranoid: true
        });

    return SalesOrder;
};

exports.createSOCustomerFromSOCustomerOld = (SalesOrder) => {
    const SalesOrderNew = JSON.parse(JSON.stringify(SalesOrder));

    // Transform new SO into channel_type MP & channel 4
    SalesOrderNew.channel_type = 'MP';
    SalesOrderNew.channel = 4;

    return SalesOrderNew;
};

exports.createSOBCIFromPOBCI = (PRBCI, POBCI, POItemsBCI) => {
    // console.log(POBCI.status);
    // console.log(getSOStatusFromPO(POBCI.status));
    return {
        purchase_request_id: PRBCI.id,
        purchase_request_no: PRBCI.purchase_request_no,
        sales_order_no: generateSONumber(),
        status: getSOStatusFromPO(POBCI.status),
        group_id: POBCI.group_id,
        ref: POBCI.ref,
        channel: 4,
        channel_type: 'MP',
        payment_method_id: POBCI.payment_method_id,
        payment_type_id: POBCI.payment_type_id,
        company_buyer_id: process.env.BCI_ID,
        company_buyer_name: process.env.BCI_NAME,
        is_partial: POBCI.is_partial,
        notes: null,
        discount: POBCI.discount,
        shipping_cost: POBCI.shipping_cost,
        tax: POBCI.tax,
        subtotal: POBCI.subtotal,
        so_amount_total: POBCI.po_amount_total,
        so_total_sku: POBCI.po_total_sku,
        term_of_payment: POBCI.term_of_payment,
        hub_id: 0,
        shipping_method: POItemsBCI[0].shipping_method,
        shipping_provider: POItemsBCI[0].shipping_provider,
        shipping_address_id: POItemsBCI[0].shipping_address_id ? POItemsBCI[0].shipping_address_id : 99999,
        shipping_country: POItemsBCI[0].shipping_country,
        shipping_province: POItemsBCI[0].shipping_province,
        shipping_city: POItemsBCI[0].shipping_city,
        shipping_zipcode: POItemsBCI[0].shipping_zipcode,
        shipping_address: POItemsBCI[0].shipping_address,
        shipping_phone: POItemsBCI[0].shipping_phone,
        shipping_phone_prefix: POItemsBCI[0].shipping_phone_prefix,
        shipping_district: POItemsBCI[0].shipping_district,
        shipping_receiver_name: POItemsBCI[0].shipping_receiver_name,
        shipping_receiver_email: POItemsBCI[0].shipping_receiver_email,
        billing_address_id: POItemsBCI[0].billing_address_id ? POItemsBCI[0].billing_address_id : 00000,
        billing_country: POItemsBCI[0].billing_country,
        billing_province: POItemsBCI[0].billing_province,
        billing_city: POItemsBCI[0].billing_city,
        billing_zipcode: POItemsBCI[0].billing_zipcode,
        billing_address: POItemsBCI[0].billing_address,
        billing_phone: POItemsBCI[0].billing_phone,
        billing_phone_prefix: POItemsBCI[0].billing_phone_prefix,
        billing_district: POItemsBCI[0].billing_district,
        billing_receiver_name: POItemsBCI[0].billing_receiver_name,
        billing_receiver_email: POItemsBCI[0].billing_receiver_email,
        delivery_notes: POItemsBCI[0].delivery_notes,
        created_by: POBCI.created_by,
        creator_type: 'ORGANIZATION',
        expired_at: POBCI.expired_at,
        created_at: POBCI.created_at,
        updated_at: POBCI.updated_at,
        deleted_at: POBCI.deleted_at
    };
};

module.exports = exports;