const { POTables, POTransaction } = require('../db_bootstrap');
const { getOffset } = require('../helpers/pagination_helpers');

const Promise = require('bluebird');

exports.count = async (where) => {
    return POTables.PurchaseOrderItem.count({
        where
    });
};

exports.findAll = async (where, limit = 20, page = 1) => {
    return POTables.PurchaseOrderItem.findAll({
        where,
        offset: getOffset(page, limit),
        limit
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAllNoLimit = async (where) => {
    return POTables.PurchaseOrderItem.findAll({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.create = async (PurchaseOrderItem) => {
    return POTables.PurchaseOrderItem.create(PurchaseOrderItem, {
        transaction: POTransaction
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.createMany = async (PurchaseOrderItems) => {
    return Promise.map(PurchaseOrderItems, (PurchaseOrderItem) => {
        return this.create(PurchaseOrderItem);
    });
};

exports.deleteByPurchaseOrderId = async (purchaseOrderId) => {
    return POTables.PurchaseOrderItem.destroy({
        where: {
            purchase_order_id: purchaseOrderId
        },
        force: true
    }, {
            transaction: POTransaction
        });
};

module.exports = exports;