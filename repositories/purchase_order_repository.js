const { POTables, POTransaction } = require('../db_bootstrap');
const { getOffset } = require('../helpers/pagination_helpers');

exports.count = async (where) => {
    return POTables.PurchaseOrder.count({
        where
    });
};

exports.findOne = async (where) => {
    return POTables.PurchaseOrder.findOne({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAll = async (where, limit = 20, page = 1) => {
    return POTables.PurchaseOrder.findAll({
        where,
        offset: getOffset(page, limit),
        limit
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAllNoLimit = async (where) => {
    return POTables.PurchaseOrder.findAll({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.create = async (PurchaseOrder) => {
    return POTables.PurchaseOrder.create(PurchaseOrder, {
        transaction: POTransaction
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.delete = async (id) => {
    return POTables.PurchaseOrder.destroy({
        where: {
            id
        },
        force: true
    }, {
            transaction: POTransaction
        });
};

module.exports = exports;