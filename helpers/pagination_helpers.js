exports.getOffset = function (page, perPage) {
    const pPage = page ? parseInt(page, 10) : 1;
    const pLlimit = perPage ? parseInt(perPage, 10) : 10;
    let offset = ((pPage - 1) * pLlimit);
    offset = offset || 0;
    return offset;
};

module.exports = exports;