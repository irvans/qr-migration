const Sequelize = require('sequelize');
const MongoClient = require('mongodb').MongoClient;

const POConn = new Sequelize(process.env.PURCHASE_ORDER_DATABASE, {
    logging: false // Disable query logging into console
});

const PRConn = new Sequelize(process.env.PURCHASE_REQUEST_DATABASE, {
    logging: false // Disable query logging into console
});

let POTransaction, PRTransaction, PRMongoConn = null;

let POTables = {
    PurchaseOrder: require('./models/purchase-order/purchase_order').model(Sequelize, POConn),
    PurchaseOrderItem: require('./models/purchase-order/purchase_order_item').model(Sequelize, POConn),
    SalesOrder: require('./models/purchase-order/sales_order').model(Sequelize, POConn),
    SalesOrderItem: require('./models/purchase-order/sales_order_item').model(Sequelize, POConn)
};

let PRTables = {
    PurchaseRequest: require('./models/purchase-request/purchase_request').model(Sequelize, PRConn),
    Quotation: require('./models/purchase-request/quotation').model(Sequelize, PRConn),
    QuotationItem: require('./models/purchase-request/quotation_item').model(Sequelize, PRConn),
};

const connectAll = async () => {
    const url = `mongodb://${process.env.PURCHASE_REQUEST_MONGODB_HOST}:${process.env.PURCHASE_REQUEST_MONGODB_PORT}/${process.env.PURCHASE_REQUEST_MONGODB_DB}`;
    const client = new MongoClient(url, {
        useNewUrlParser: true,
        auth: {
            user: process.env.PURCHASE_REQUEST_MONGODB_USER,
            password: process.env.PURCHASE_REQUEST_MONGODB_PASSWORD
        }
    });

    await client.connect()
        .then((client) => {
            console.log('PR Mongo Connected');
            PRMongoConn = client.db(process.env.PURCHASE_REQUEST_MONGODB_DB);
        });

    await POConn.authenticate()
        .then(() => {
            console.log('PO Connected');
        })
        .catch((err) => {
            console.error('err', err);
        });

    await PRConn.authenticate()
        .then(() => {
            console.log('PR Connected');
        })
        .catch((err) => {
            console.error('err', err);
        });
};

const setPOTransaction = (transaction) => {
    POTransaction = transaction;
};

const setPRTransaction = (transaction) => {
    PRTransaction = transaction;
};

const getPRMongoConn = () => {
    return PRMongoConn;
};

module.exports = {
    POConn,
    PRConn,
    getPRMongoConn,
    POTables,
    PRTables,
    connectAll,
    setPOTransaction,
    setPRTransaction,
    PRTransaction,
    POTransaction
};