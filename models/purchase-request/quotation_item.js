const Promise = require('bluebird');

exports.model = (Sequelize, connection) => {
    const QuotationItem = connection.define('QuotationItem', {
        id: {
            type: Sequelize.BIGINT,
            allowNull: true,
            primaryKey: true,
            autoIncrement: true
        },
        quotation_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        revision_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        company_product_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        company_seller_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        company_seller_name: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        purchase_order_no_internal: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        warehouse_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        hub_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        is_partial: {
            type: Sequelize.BOOLEAN,
            allowNull: true
        },
        is_pkp: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        shopping_cart_item_id: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        sku_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        sku_no: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        budget_id: {
            type: Sequelize.BIGINT,
            allowNull: true,
            defaultValue: '0'
        },
        qty: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        base_price: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        price: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        margin: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        added_price: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        deducted_price: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        discount: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        tax: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        shipping_cost: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        shipping_method: {
            type: Sequelize.INTEGER(11).UNSIGNED,
            allowNull: true,
            defaultValue: '0'
        },
        shipping_price_estimation: {
            type: Sequelize.DECIMAL,
            allowNull: false,
            defaultValue: '0'
        },
        shipping_country: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_province: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_city: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_zipcode: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_address_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        shipping_address: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_phone: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_phone_prefix: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_district: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_receiver_name: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_address_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        billing_country: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_province: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_city: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_zipcode: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_address: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_phone: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_phone_prefix: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_district: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_receiver_name: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_receiver_email: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        status: {
            type: Sequelize.INTEGER(3).UNSIGNED,
            allowNull: true,
            defaultValue: '50'
        },
        status_reason: {
            type: Sequelize.INTEGER(11),
            allowNull: true,
            defaultValue: '0'
        },
        reason_note: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        created_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        created_by: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        updated_at: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
        },
        deleted_at: {
            type: Sequelize.DATE,
            allowNull: true
        }
    }, {
            tableName: 'quotation_items',
            freezeTableName: true,
            underscored: true
        });

    return QuotationItem;
};

exports.createQuotationItemBCIFromPOBCI = (
    QuotationBCI,
    POBCI,
    POItemsBCI,
    originalCompanySellerId,
    originalCompanySellerName
) => {
    return Promise.map(POItemsBCI, (POItemBCI) => {
        return {
            quotation_id: QuotationBCI.id,
            revision_id: null,
            company_product_id: POItemBCI.company_product_id,
            company_seller_id: originalCompanySellerId,
            company_seller_name: originalCompanySellerName,
            purchase_order_no_internal: POBCI.purchase_order_no,
            warehouse_id: POItemBCI.warehouse_id,
            hub_id: POItemBCI.hub_id,
            is_partial: POItemBCI.is_partial,
            is_pkp: POItemBCI.is_pkp,
            shopping_cart_item_id: '#####',
            sku_id: POItemBCI.sku_id,
            sku_no: POItemBCI.sku_no,
            budget_id: POItemBCI.budget_id,
            qty: POItemBCI.qty,
            base_price: POItemBCI.base_price,
            price: POItemBCI.price,
            margin: POItemBCI.margin,
            added_price: POItemBCI.added_price,
            deducted_price: POItemBCI.deducted_price,
            discount: POItemBCI.discount,
            tax: POItemBCI.tax,
            shipping_cost: POItemBCI.shipping_cost,
            shipping_method: POItemBCI.shipping_method,
            shipping_price_estimation: POItemBCI.shipping_price_estimation ? POItemBCI.shipping_price_estimation : '0.00',
            shipping_country: POItemBCI.shipping_country,
            shipping_province: POItemBCI.shipping_province,
            shipping_city: POItemBCI.shipping_city,
            shipping_zipcode: POItemBCI.shipping_zipcode,
            shipping_address_id: POItemBCI.shipping_address_id,
            shipping_address: POItemBCI.shipping_address,
            shipping_phone: POItemBCI.shipping_phone,
            shipping_phone_prefix: POItemBCI.shipping_phone_prefix,
            shipping_district: POItemBCI.shipping_district,
            shipping_receiver_name: POItemBCI.shipping_receiver_name,
            billing_address_id: POItemBCI.billing_address_id,
            billing_country: POItemBCI.billing_country,
            billing_province: POItemBCI.billing_province,
            billing_city: POItemBCI.billing_city,
            billing_zipcode: POItemBCI.billing_zipcode,
            billing_address: POItemBCI.billing_address,
            billing_phone: POItemBCI.billing_phone,
            billing_phone_prefix: POItemBCI.billing_phone_prefix,
            billing_district: POItemBCI.billing_district,
            billing_receiver_name: POItemBCI.billing_receiver_name,
            billing_receiver_email: POItemBCI.billing_receiver_email,
            status: POItemBCI.status,
            status_reason: POItemBCI.status_reason,
            reason_note: POItemBCI.reason_note,
            created_at: POItemBCI.created_at,
            created_by: POBCI.created_by,
            updated_at: POItemBCI.updated_at,
            deleted_at: POItemBCI.deleted_at
        };
    });
};

exports.createQuotationItemSnapshotBCIFromPOBCI = (
    QuotationBCI,
    POBCI,
    POItemsBCI,
    SOCustomer,
    originalCompanySellerId,
    originalCompanySellerName
) => {
    return Promise.map(POItemsBCI, (POItemBCI) => {
        return {
            quotation_id: QuotationBCI.id,
            revision_id: null,
            company_product_id: POItemBCI.company_product_id,
            company_seller_id: originalCompanySellerId,
            company_seller_name: originalCompanySellerName,
            purchase_order_no_internal: POBCI.purchase_order_no,
            warehouse_id: POItemBCI.warehouse_id,
            hub_id: SOCustomer.hub_id,
            is_partial: POBCI.is_partial,
            is_pkp: (POItemBCI.tax ? 1 : 0),
            sku_id: POItemBCI.sku_id,
            sku_no: POItemBCI.sku_no,
            budget_id: POItemBCI.budget_id,
            qty: POItemBCI.qty,
            base_price: POItemBCI.base_price,
            price: POItemBCI.price,
            margin: POItemBCI.margin,
            added_price: POItemBCI.added_price,
            deducted_price: POItemBCI.deducted_price,
            discount: POItemBCI.discount,
            tax: POItemBCI.tax,
            shipping_cost: POItemBCI.shipping_cost,
            shipping_method: POItemBCI.shipping_method,
            shipping_price_estimation: POItemBCI.shipping_price_estimation ? POItemBCI.shipping_price_estimation : '0.00',
            shipping_country: POItemBCI.shipping_country,
            shipping_province: POItemBCI.shipping_province,
            shipping_city: POItemBCI.shipping_city,
            shipping_zipcode: POItemBCI.shipping_zipcode,
            shipping_address_id: POItemBCI.shipping_address_id ? POItemBCI.shipping_address_id : 99999,
            shipping_address: POItemBCI.shipping_address,
            shipping_phone: POItemBCI.shipping_phone,
            shipping_phone_prefix: POItemBCI.shipping_phone_prefix,
            shipping_district: POItemBCI.shipping_district,
            shipping_receiver_name: POItemBCI.shipping_receiver_name,
            billing_address_id: POItemBCI.billing_address_id,
            billing_country: POItemBCI.billing_country,
            billing_province: POItemBCI.billing_province,
            billing_city: POItemBCI.billing_city,
            billing_zipcode: POItemBCI.billing_zipcode,
            billing_address: POItemBCI.billing_address,
            billing_phone: POItemBCI.billing_phone,
            billing_phone_prefix: POItemBCI.billing_phone_prefix,
            billing_district: POItemBCI.billing_district,
            billing_receiver_name: POItemBCI.billing_receiver_name,
            billing_receiver_email: POItemBCI.billing_receiver_email,
            status: POItemBCI.status,
            status_reason: POItemBCI.status_reason,
            reason_note: POItemBCI.reason_note,
            created_at: POItemBCI.created_at,
            created_by: POBCI.created_by,
            updated_at: POItemBCI.updated_at,
            deleted_at: POItemBCI.deleted_at
        };
    });
};


module.exports = exports;