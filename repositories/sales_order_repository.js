const { POTables, POTransaction } = require('../db_bootstrap');
const { getOffset } = require('../helpers/pagination_helpers');

exports.count = async (where) => {
    return POTables.SalesOrder.count({
        where
    });
};

exports.findOne = async (where) => {
    return POTables.SalesOrder.findOne({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAll = async (where, limit = 20, page = 1, order = [['id', 'ASC']]) => {
    return POTables.SalesOrder.findAll({
        where,
        offset: getOffset(page, limit),
        limit,
        order
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAllNoLimit = async (where) => {
    return POTables.SalesOrder.findAll({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.create = async (SalesOrder) => {
    return POTables.SalesOrder.create(SalesOrder, {
        transaction: POTransaction
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.updateToMP = async (salesOrderId) => {
    return POTables.SalesOrder.update({
        channel: 4,
        channel_type: 'MP'
    }, {
        where: {
            id: salesOrderId
        },
        transaction: POTransaction
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.delete = async (id) => {
    return POTables.SalesOrder.destroy({
        where: {
            id
        },
        force: true
    }, {
        transaction: POTransaction
    });
};

module.exports = exports;