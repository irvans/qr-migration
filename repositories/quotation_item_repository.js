const { PRTables, PRTransaction } = require('../db_bootstrap');
const { getOffset } = require('../helpers/pagination_helpers');
const Promise = require('bluebird');

exports.count = async (where) => {
    return PRTables.QuotationItem.count({
        where
    });
};

exports.findOne = async (where) => {
    return PRTables.QuotationItem.findOne({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAll = async (where, limit = 20, page = 1) => {
    return PRTables.QuotationItem.findAll({
        where,
        offset: getOffset(page, limit),
        limit
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAllNoLimit = async (where) => {
    return PRTables.QuotationItem.findAll({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.create = async (QuotationItem) => {
    return PRTables.QuotationItem.create(QuotationItem, {
        transaction: PRTransaction
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.createMany = async (QuotationItems) => {
    return Promise.map(QuotationItems, (QuotationItem) => {
        return this.create(QuotationItem);
    }, {
        concurrency: 1
    });
};

module.exports = exports;