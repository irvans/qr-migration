const AWS = require('aws-sdk');

exports.call = async (serviceName, action, data) => {
    const lambda = new AWS.Lambda({
        region: 'ap-southeast-1'
    });

    const mappingData = {
        action,
        context: {},
        data
    };

    console.log(mappingData);
    const lambdaPayload = JSON.stringify(mappingData);
    const invokeParam = {
        FunctionName: serviceName,
        InvocationType: 'RequestResponse',
        Payload: lambdaPayload
    };

    console.log(`CallTo: ${serviceName} - ${action}`);

    const response = await lambda.invoke(invokeParam).promise().catch((error) => {
        throw new Error(error.message);
    });

    const result = JSON.parse(response.Payload);
    if (result.errorMessage) {
        let parsedError;
        try {
            parsedError = JSON.parse(result.errorMessage);
        } catch (err) {
            console.log(err);
        }

        if (parsedError) {
            throw new Error(JSON.stringify({
                code: parsedError.code,
                message: parsedError.detail
            }));
        }

        throw new Error(JSON.stringify({
            code: 'BadRequest',
            message: result.errorMessage
        }));
    }

    return result;
};

module.exports = exports;