const { POTables, POTransaction } = require('../db_bootstrap');
const { getOffset } = require('../helpers/pagination_helpers');
const Promise = require('bluebird');

exports.count = async (where) => {
    return POTables.SalesOrderItem.count({
        where
    });
};

exports.findAll = async (where, limit = 20, page = 1) => {
    return POTables.SalesOrderItem.findAll({
        where,
        offset: getOffset(page, limit),
        limit
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAllNoLimit = async (where) => {
    return POTables.SalesOrderItem.findAll({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.create = async (SalesOrderItem) => {
    return POTables.SalesOrderItem.create(SalesOrderItem, {
        transaction: POTransaction
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.createMany = async (SalesOrderItems) => {
    return Promise.map(SalesOrderItems, (SalesOrderItem) => {
        return this.create(SalesOrderItem);
    });
};

exports.deleteBySalesOrderId = async (salesOrderId) => {
    return POTables.SalesOrderItem.destroy({
        where: {
            sales_order_id: salesOrderId
        },
        force: true
    }, {
        transaction: POTransaction
    });
};

module.exports = exports;