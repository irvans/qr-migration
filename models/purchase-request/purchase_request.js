const { PO_STATUS_TO_PR_STATUS } = require('./../../helpers/status_mapper');

exports.model = (Sequelize, connection) => {
    const PurchaseRequest = connection.define(
        'PurchaseRequest', {
            id: {
                type: Sequelize.BIGINT,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            quotation_id: {
                type: Sequelize.BIGINT,
                allowNull: false
            },
            purchase_order_id: {
                type: Sequelize.BIGINT,
                allowNull: true
            },
            revision_id: {
                type: Sequelize.BIGINT,
                allowNull: true
            },
            purchase_request_no: {
                type: Sequelize.STRING(255),
                allowNull: false
            },
            purchase_request_internal: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            group_id: {
                type: Sequelize.INTEGER,
                allowNull: true
            },
            status: {
                type: Sequelize.INTEGER(3).UNSIGNED,
                allowNull: false,
                defaultValue: '10'
            },
            ref: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            channel: {
                type: Sequelize.SMALLINT,
                allowNull: true
            },
            channel_type: {
                type: Sequelize.ENUM('QR', 'MP'),
                allowNull: false
            },
            payment_method_id: {
                type: Sequelize.STRING(10),
                allowNull: false
            },
            payment_type_id: {
                type: Sequelize.STRING(10),
                allowNull: false
            },
            term_of_payment: {
                type: Sequelize.BIGINT,
                allowNull: true
            },
            company_buyer_id: {
                type: Sequelize.BIGINT,
                allowNull: false
            },
            company_buyer_name: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            notes: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            discount: {
                type: Sequelize.DECIMAL,
                allowNull: true
            },
            tax: {
                type: Sequelize.DECIMAL,
                allowNull: true
            },
            shipping_cost: {
                type: Sequelize.DECIMAL,
                allowNull: true
            },
            subtotal: {
                type: Sequelize.DECIMAL,
                allowNull: true
            },
            pr_amount_total: {
                type: Sequelize.DECIMAL,
                allowNull: true
            },
            punchout_file_url: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            created_by: {
                type: Sequelize.BIGINT,
                allowNull: true
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: true
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: true,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            deleted_at: {
                type: Sequelize.DATE,
                allowNull: true
            },
            expired_at: {
                type: Sequelize.DATE,
                allowNull: true
            }
        },
        {
            tableName: 'purchase_requests',
            freezeTableName: true,
            underscored: true,
            paranoid: true
        }
    );

    PurchaseRequest.prototype.getStatus = function getStatus() {
        switch (parseInt(this.status, 10)) {
            case 10:
                return 'Menunggu Persetujuan';
            case 20:
                return 'Disetujui';
            case 25:
                return 'Kadaluarsa';
            case 30:
                return 'Ditolak';
            case 40:
                return 'Dibatalkan';
            default:
                return 'Disetujui';
        }
    };

    return PurchaseRequest;
};

exports.createPRBCICustomerFromPOBCI = (QuotationBCI, POBCI) => {
    return {
        quotation_id: QuotationBCI.id,
        purchase_order_id: POBCI.id,
        revision_id: null,
        purchase_request_no: `PR-${new Date().getTime()}`,
        purchase_request_internal: null,
        group_id: POBCI.group_id,
        status: PO_STATUS_TO_PR_STATUS[POBCI.status],
        ref: POBCI.ref,
        channel: 4,
        channel_type: 'MP',
        payment_method_id: POBCI.payment_method_id,
        payment_type_id: POBCI.payment_type_id,
        term_of_payment: POBCI.term_of_payment,
        company_buyer_id: process.env.BCI_ID,
        company_buyer_name: process.env.BCI_NAME,
        notes: POBCI.notes,
        discount: POBCI.discount,
        tax: POBCI.tax,
        shipping_cost: POBCI.shipping_cost,
        subtotal: POBCI.subtotal,
        pr_amount_total: POBCI.po_amount_total,
        punchout_file_url: null,
        created_by: POBCI.created_by,
        created_at: POBCI.created_at,
        updated_at: POBCI.updated_at,
        deleted_at: POBCI.deleted_at,
        expired_at: POBCI.expired_at
    }
};

module.exports = exports;