const uuidv4 = require('uuid/v4');

exports.model = (Sequelize, connection) => {
    const SalesOrderItems = connection.define('SalesOrderItems', {
        id: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.BIGINT,
            title: "",
            formType: "TEXT",
            autoIncrement: true
        },
        sales_order_id: {
            allowNull: false,
            type: Sequelize.BIGINT,
            title: "",
            formType: "TEXT",
            references: {
                model: 'sales_order',
                key: 'id'
            }
        },
        purchase_order_item_pair_id: {
            allowNull: true,
            type: Sequelize.STRING(36),
            title: "",
            formType: "TEXT"
        },
        sku_no: {
            allowNull: false,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        sku_id: {
            allowNull: false,
            defaultValue: '0',
            type: Sequelize.INTEGER(11),
            title: "",
            formType: "TEXT"
        },
        sku_description: {
            allowNull: false,
            type: Sequelize.TEXT,
            title: "",
            formType: "TEXT"
        },
        status: {
            allowNull: false,
            defaultValue: '10',
            type: Sequelize.INTEGER(3).UNSIGNED,
            title: "",
            formType: "TEXT"
        },
        qty: {
            allowNull: false,
            type: Sequelize.DECIMAL,
            title: "",
            formType: "TEXT"
        },
        unit_id: {
            allowNull: true,
            type: Sequelize.INTEGER(11),
            title: "",
            formType: "TEXT"
        },
        base_price: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "Sales Price before Tax",
            formType: "TEXT"
        },
        margin: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "Base Margin Amount = Dari PE",
            formType: "TEXT"
        },
        added_price: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "base_price + margin",
            formType: "TEXT"
        },
        discount: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "discount if any (rebate)",
            formType: "TEXT"
        },
        deducted_price: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "added_price - discount",
            formType: "TEXT"
        },
        tax: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "Items Tax = Math.floor(Rate*Qty*0.1)",
            formType: "TEXT"
        },
        top_adjustment: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "TOP Adjustment =  Cari Sendiri",
            formType: "TEXT"
        },
        sales_adjustment: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "Sales Adjustment = Cari Sendiri ",
            formType: "TEXT"
        },
        shipping_cost: {
            allowNull: false,
            type: Sequelize.DECIMAL,
            title: "",
            formType: "TEXT"
        },
        shipping_cost_tax: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "",
            formType: "TEXT"
        },
        price: {
            allowNull: false,
            defaultValue: '0.00',
            type: Sequelize.DECIMAL,
            title: "Gross Amount = (Rate*Qty) + Tax",
            formType: "TEXT"
        },
        notes: {
            allowNull: true,
            type: Sequelize.TEXT,
            title: "",
            formType: "TEXT"
        },
        shipping_date: {
            allowNull: true,
            type: Sequelize.DATE,
            title: "",
            formType: "DATE"
        },
        shipping_airway_bill: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipping_airway_bill_image: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipper_name: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        shipper_phone: {
            allowNull: true,
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        recipient: {
            allowNull: true,
            defaultValue: '',
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        recipient_phone_no: {
            allowNull: true,
            defaultValue: '',
            type: Sequelize.STRING(255),
            title: "",
            formType: "TEXT"
        },
        created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            type: Sequelize.DATE,
            title: "",
            formType: "DATE"
        },
        updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            type: Sequelize.DATE,
            title: "",
            formType: "DATE"
        },
        deleted_at: {
            allowNull: true,
            type: Sequelize.DATE,
            title: "",
            formType: "DATE"
        }
    }, {
            tableName: 'sales_order_items',
            freezeTableName: true,
            underscored: true,
            paranoid: true
        });

    return SalesOrderItems;
};

exports.createSOItemsCustomerFromSOItemsCustomerOld = (SOCustomer, SOItemsCustomer) => {
    return SOItemsCustomer.map((SOItemCustomer) => {
        return {
            sales_order_id: SOCustomer.id,
            purchase_order_item_pair_id: SOItemCustomer.purchase_order_item_pair_id,
            sku_no: SOItemCustomer.sku_no,
            sku_id: SOItemCustomer.sku_id,
            sku_description: SOItemCustomer.sku_description,
            status: SOCustomer.status,
            qty: SOItemCustomer.qty,
            unit_id: SOItemCustomer.unit_id,
            base_price: SOItemCustomer.base_price,
            margin: SOItemCustomer.base_price,
            added_price: SOItemCustomer.added_price,
            discount: SOItemCustomer.discount,
            deducted_price: SOItemCustomer.deducted_price,
            tax: SOItemCustomer.tax,
            top_adjustment: SOItemCustomer.top_adjustment,
            sales_adjustment: SOItemCustomer.sales_adjustment,
            shipping_cost: SOItemCustomer.shipping_cost,
            shipping_cost_tax: SOItemCustomer.shipping_cost_tax,
            price: SOItemCustomer.price,
            notes: SOItemCustomer.notes,
            shipping_date: SOItemCustomer.shipping_date,
            shipping_airway_bill: SOItemCustomer.shipping_airway_bill,
            shipping_airway_bill_image: SOItemCustomer.shipping_airway_bill_image,
            shipper_name: SOItemCustomer.shipper_name,
            shipper_phone: SOItemCustomer.shipper_phone,
            recipient: SOItemCustomer.recipient,
            recipient_phone_no: SOItemCustomer.recipient_phone_no,
            created_at: SOItemCustomer.created_at,
            updated_at: SOItemCustomer.updated_at,
            deleted_at: SOItemCustomer.deleted_at
        };
    });
};

exports.createSOItemBCIFromPOItemBCI = (SOBCI, POItemsBCI) => {
    return POItemsBCI.map((POItemBCI) => {
        const pairUUID = uuidv4();

        POItemBCI.new_pair_id = pairUUID;
        return {
            sales_order_id: SOBCI.id,
            purchase_order_item_pair_id: pairUUID,
            sku_no: POItemBCI.sku_no,
            sku_id: POItemBCI.sku_id,
            sku_description: '',
            status: SOBCI.status,
            qty: POItemBCI.qty,
            unit_id: POItemBCI.unit_id,
            base_price: POItemBCI.base_price,
            margin: POItemBCI.base_price,
            added_price: POItemBCI.added_price,
            discount: POItemBCI.discount,
            deducted_price: POItemBCI.deducted_price,
            tax: POItemsBCI.tax,
            top_adjustment: '0.00',
            sales_adjustment: '0.00',
            shipping_cost: POItemBCI.shipping_cost,
            shipping_cost_tax: '0.00',
            price: POItemBCI.price,
            notes: POItemBCI.notes,
            shipping_date: POItemBCI.shipping_date,
            shipping_airway_bill: POItemBCI.shipping_airway_bill,
            shipping_airway_bill_image: POItemBCI.shipping_airway_bill_image,
            shipper_name: POItemBCI.shipper_name,
            shipper_phone: POItemBCI.shipper_phone,
            recipient: POItemBCI.recipient,
            recipient_phone_no: POItemBCI.recipient_phone_no,
            created_at: POItemBCI.created_at,
            updated_at: POItemBCI.updated_at,
            deleted_at: POItemBCI.deleted_at
        };
    });
};

module.exports = exports;