exports.PURCHASE_ORDER_STATUS = {
    WAITING_CUSTOMER_APPROVAL: 48,
    WAITING_PAYMENT: 49,
    NEW_ORDER: 50,
    APPROVED: 60,
    IN_DELIVERY: 70,
    RECEIVED: 75,
    RECEIVED_ACCEPTED_BY_CUSTOMER: 80,
    RECEIVED_REJECTED_BY_CUSTOMER: 81,
    REJECTED_BY_VENDOR_APPROVAL: 90,
    REJECTED_BY_CUSTOMER_APPROVAL: 91,
    REJECTED_BY_VENDOR_SHIPMENT: 92,
    EXPIRED_BY_VENDOR_APPROVAL: 100,
    EXPIRED_BY_CUSTOMER_PAYMENT: 105,
    EXPIRED_BY_VENDOR_DELIVERY: 106,
    INVOICING: 120,
    PAYMENT_SUCCESS: 130
};

exports.PURCHASE_ORDER_STATUS_TO_STRING = {
    48: 'WAITING_CUSTOMER_APPROVAL',
    49: 'WAITING_PAYMENT',
    50: 'NEW_ORDER',
    60: 'APPROVED',
    70: 'IN_DELIVERY',
    75: 'RECEIVED',
    80: 'RECEIVED_ACCEPTED_BY_CUSTOMER',
    81: 'RECEIVED_REJECTED_BY_CUSTOMER',
    90: 'REJECTED_BY_VENDOR_APPROVAL',
    91: 'REJECTED_BY_CUSTOMER_APPROVAL',
    92: 'REJECTED_BY_VENDOR_SHIPMENT',
    100: 'EXPIRED_BY_VENDOR_APPROVAL',
    105: 'EXPIRED_BY_CUSTOMER_PAYMENT',
    106: 'EXPIRED_BY_VENDOR_DELIVERY',
    120: 'INVOICING',
    130: 'PAYMENT_SUCCESS'
};

exports.SALES_ORDER_STATUS = {
    WAITING_CUSTOMER_APPROVAL: 480,
    WAITING_PAYMENT: 490,
    WAITING_PAYMENT_VERIFICATION: 495,
    WAITING_CONFIRM_VENDOR: 500,
    PROCESSING: 600,
    IN_DELIVERY: 700,
    SENT: 750,
    RECEIVED_ACCEPTED_BY_CUSTOMER: 800,
    RECEIVED_REJECTED_BY_CUSTOMER: 810,
    REJECTED_BY_VENDOR_APPROVAL: 900,
    REJECTED_BY_CUSTOMER_APPROVAL: 910,
    REJECTED_BY_VENDOR_SHIPMENT: 920,
    EXPIRED_BY_VENDOR_APPROVAL: 1000,
    EXPIRED_BY_CUSTOMER_PAYMENT: 1050,
    EXPIRED_BY_VENDOR_DELIVERY: 1060,
    INVOICING: 1200, // INVOICING
    PAYMENT_SUCCESS: 1300
};

exports.SALES_ORDER_STATUS_TO_STRING = {
    480: 'WAITING_CUSTOMER_APPROVAL',
    490: 'WAITING_PAYMENT',
    495: 'WAITING_PAYMENT_VERIFICATION',
    500: 'WAITING_CONFIRM_VENDOR',
    600: 'PROCESSING',
    700: 'IN_DELIVERY',
    750: 'SENT',
    800: 'RECEIVED_ACCEPTED_BY_CUSTOMER',
    810: 'RECEIVED_REJECTED_BY_CUSTOMER',
    900: 'REJECTED_BY_VENDOR_APPROVAL',
    910: 'REJECTED_BY_CUSTOMER_APPROVAL',
    920: 'REJECTED_BY_VENDOR_SHIPMENT',
    1000: 'EXPIRED_BY_VENDOR_APPROVAL',
    1050: 'EXPIRED_BY_CUSTOMER_PAYMENT',
    1060: 'EXPIRED_BY_VENDOR_DELIVERY',
    1200: 'INVOICING',
    1300: 'PAYMENT_SUCCESS'
};

exports.PO_STATUS_TO_PR_STATUS = {
    48: 10,
    49: 15,
    50: 15,
    60: 20,
    70: 20,
    75: 20,
    80: 20,
    81: 20,
    90: 20,
    91: 20,
    92: 30,
    100: 20,
    105: 20,
    106: 20,
    120: 20,
    130: 20
};

exports.STATUS_MAP = (status) => {
    let salesOrder;
    let purchaseOrder;

    switch (status) {
        case 'WAITING_CUSTOMER_APPROVAL': {
            salesOrder = exports.SALES_ORDER_STATUS.WAITING_CUSTOMER_APPROVAL;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.WAITING_CUSTOMER_APPROVAL;
            break;
        }

        case 'WAITING_PAYMENT': {
            salesOrder = exports.SALES_ORDER_STATUS.WAITING_PAYMENT;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.WAITING_PAYMENT;
            break;
        }

        case 'WAITING_CONFIRM_VENDOR':
        case 'NEW_ORDER': {
            salesOrder = exports.SALES_ORDER_STATUS.WAITING_CONFIRM_VENDOR;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.NEW_ORDER;
            break;
        }

        case 'PROCESSING':
        case 'APPROVED': {
            salesOrder = exports.SALES_ORDER_STATUS.PROCESSING;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.APPROVED;
            break;
        }

        case 'IN_DELIVERY': {
            salesOrder = exports.SALES_ORDER_STATUS.IN_DELIVERY;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.IN_DELIVERY;
            break;
        }

        case 'SENT':
        case 'RECEIVED': {
            salesOrder = exports.SALES_ORDER_STATUS.SENT;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.RECEIVED;
            break;
        }

        case 'RECEIVED_REJECTED_BY_CUSTOMER': {
            salesOrder = exports.SALES_ORDER_STATUS.RECEIVED_REJECTED_BY_CUSTOMER;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.RECEIVED_REJECTED_BY_CUSTOMER;
            break;
        }

        case 'ACCEPTED_CUSTOMER':
        case 'RECEIVED_ACCEPTED_BY_CUSTOMER': {
            salesOrder = exports.SALES_ORDER_STATUS.RECEIVED_ACCEPTED_BY_CUSTOMER;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.RECEIVED_ACCEPTED_BY_CUSTOMER;
            break;
        }

        case 'REJECTED_BY_VENDOR_APPROVAL': {
            salesOrder = exports.SALES_ORDER_STATUS.REJECTED_BY_VENDOR_APPROVAL;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.REJECTED_BY_VENDOR_APPROVAL;
            break;
        }

        case 'CANCELLED_BY_CUSTOMER': {
            salesOrder = exports.SALES_ORDER_STATUS.REJECTED_BY_CUSTOMER_APPROVAL;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.REJECTED_BY_CUSTOMER_APPROVAL;
            break;
        }

        case 'REJECTED_BY_CUSTOMER_APPROVAL': {
            salesOrder = exports.SALES_ORDER_STATUS.REJECTED_BY_CUSTOMER_APPROVAL;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.REJECTED_BY_CUSTOMER_APPROVAL;
            break;
        }

        case 'EXPIRED_BY_VENDOR_APPROVAL': {
            salesOrder = exports.SALES_ORDER_STATUS.EXPIRED_BY_VENDOR_APPROVAL;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.EXPIRED_BY_VENDOR_APPROVAL;
            break;
        }

        case 'EXPIRED_BY_CUSTOMER_PAYMENT': {
            salesOrder = exports.SALES_ORDER_STATUS.EXPIRED_BY_CUSTOMER_PAYMENT;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.EXPIRED_BY_CUSTOMER_PAYMENT;
            break;
        }

        case 'EXPIRED_BY_VENDOR_DELIVERY': {
            salesOrder = exports.SALES_ORDER_STATUS.EXPIRED_BY_VENDOR_DELIVERY;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.EXPIRED_BY_VENDOR_DELIVERY;
            break;
        }

        case 'INVOICING': {
            salesOrder = exports.SALES_ORDER_STATUS.INVOICING;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.INVOICING;
            break;
        }

        case 'PAYMENT_SUCCESS': {
            salesOrder = exports.SALES_ORDER_STATUS.PAYMENT_SUCCESS;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.PAYMENT_SUCCESS;
            break;
        }

        case 'REJECTED_BY_VENDOR_SHIPMENT': {
            salesOrder = exports.SALES_ORDER_STATUS.REJECTED_BY_VENDOR_SHIPMENT;
            purchaseOrder = exports.PURCHASE_ORDER_STATUS.REJECTED_BY_VENDOR_SHIPMENT;
            break;
        }

        default:
            break;
    }

    return {
        sales_order: salesOrder,
        purchase_order: purchaseOrder
    };
};

exports.getPOStatusFromSO = (SOStatus) => {
    const SOStatusString = this.SALES_ORDER_STATUS_TO_STRING[SOStatus];

    const statusMap = this.STATUS_MAP(SOStatusString);

    return statusMap.purchase_order;
};

exports.getSOStatusFromPO = (POStatus) => {
    const POStatusString = this.PURCHASE_ORDER_STATUS_TO_STRING[POStatus];

    const statusMap = this.STATUS_MAP(POStatusString);

    return statusMap.sales_order
};

module.exports = exports;