const { PRTables, PRTransaction } = require('../db_bootstrap');
const { getOffset } = require('../helpers/pagination_helpers');

exports.count = async (where) => {
    return PRTables.PurchaseRequest.count({
        where
    });
};

exports.findOne = async (where) => {
    return PRTables.PurchaseRequest.findOne({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAll = async (where, limit = 20, page = 1) => {
    return PRTables.PurchaseRequest.findAll({
        where,
        offset: getOffset(page, limit),
        limit
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.findAllNoLimit = async (where) => {
    return PRTables.PurchaseRequest.findAll({
        where
    }).then(result => JSON.parse(JSON.stringify(result)));
};

exports.create = async (PR) => {
    return PRTables.PurchaseRequest.create(PR, {
        transaction: PRTransaction
    }).then(result => JSON.parse(JSON.stringify(result)));
};

module.exports = exports;