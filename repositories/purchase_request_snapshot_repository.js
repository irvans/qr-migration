const { getPRMongoConn } = require('../db_bootstrap');

exports.createQuotationItemSnapshot = (data) => {
    const client = getPRMongoConn();
    return client.collection('quotation_items').insertOne(data)
        .then(result => String(result.insertedId));
};

exports.findOnePurchaseRequestSnapshot = (purchaseRequestNo) => {
    const client = getPRMongoConn();
    return client.collection('purchase_request').findOne({
        purchase_request_no: purchaseRequestNo
    })
        .then(result => JSON.parse(JSON.stringify(result)));
};