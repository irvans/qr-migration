const moment = require('moment');

exports.model = (Sequelize, connection) => {
    const Quotation = connection.define('Quotation', {
        id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        quotation_no: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        rfq_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        revision_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        company_buyer_id: {
            type: Sequelize.BIGINT,
            allowNull: false
        },
        valid_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        created_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        updated_at: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
        },
        deleted_at: {
            type: Sequelize.DATE,
            allowNull: true
        }
    }, {
            tableName: 'quotations',
            freezeTableName: true,
            underscored: true
        });

    return Quotation;
};

exports.createQuotationFromPOBCI = (POBCI, index) => {
    const now = moment();
    return {
        quotation_no: `QN-${process.env.BCI_ID}-${now.unix()}-${index}`,
        rfq_id: null,
        revision_id: null,
        company_buyer_id: process.env.BCI_ID,
        valid_date: now.add(48, 'hours'),
        created_at: now,
        updated_at: null,
        deleted_at: null
    }
};

module.exports = exports;