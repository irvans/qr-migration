exports.generatePONumber = () => {
    return `PO-${Math.floor(100 + (Math.random() * 900))}${new Date().getTime()}`;
};

exports.generateSONumber = () => {
    return `PO-${Math.floor(100 + (Math.random() * 900))}${new Date().getTime()}`;
};

module.exports = exports;