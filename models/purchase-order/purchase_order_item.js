exports.model = (Sequelize, connection) => {
    const PurchaseOrderItem = connection.define('PurchaseOrderItem', {
        id: {
            type: Sequelize.BIGINT,
            allowNull: true,
            primaryKey: true,
            autoIncrement: true
        },
        pair_id: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        purchase_order_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        revision_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        company_product_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        warehouse_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        sku_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        sku_no: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        qty: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        unit_id: {
            type: Sequelize.INTEGER(11),
            allowNull: true
        },
        base_price: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        price: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        added_price: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        deducted_price: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        margin: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        tax: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        tax_seller: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        shipping_address_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        shipping_cost: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        shipping_method: {
            type: Sequelize.INTEGER(3).UNSIGNED,
            allowNull: true,
            defaultValue: '0'
        },
        shipping_provider: {
            type: Sequelize.STRING(50),
            allowNull: true
        },
        shipping_price_estimation: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        shipping_country: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_province: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_city: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_zipcode: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_address: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_phone: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_phone_prefix: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_district: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_receiver_name: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_date: {
            type: Sequelize.DATE,
            allowNull: true
        },
        shipping_airway_bill: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipping_airway_bill_image: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipper_name: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        shipper_phone: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_address_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        billing_country: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_province: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_city: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_zipcode: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_address: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_phone: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_phone_prefix: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_district: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_receiver_name: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        billing_receiver_email: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        discount: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        recipient: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        receipt_date: {
            type: Sequelize.DATE,
            allowNull: true
        },
        delivery_order: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        delivery_order_image: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        recipient_phone_no: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        delivery_note: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        notes: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        status: {
            type: Sequelize.INTEGER(3).UNSIGNED,
            allowNull: true,
            defaultValue: '50'
        },
        created_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        updated_at: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: connection.literal('CURRENT_TIMESTAMP')
        },
        deleted_at: {
            type: Sequelize.DATE,
            allowNull: true
        }
    }, {
            tableName: 'purchase_order_items',
            freezeTableName: true,
            underscored: true,
            paranoid: true
        });

    return PurchaseOrderItem;
};

exports.createPOItemBCIFromPOItemBCIOld = (POBCINew, POBCIItemsOld) => {
    return POBCIItemsOld.map((POBCIItem) => {
        return {
            pair_id: POBCIItem.pair_id,
            purchase_order_id: POBCINew.id,
            revision_id: POBCIItem.revision_id,
            company_product_id: POBCIItem.company_product_id,
            warehouse_id: POBCIItem.warehouse_id,
            sku_id: POBCIItem.sku_id,
            sku_no: POBCIItem.sku_no,
            qty: POBCIItem.qty,
            unit_id: POBCIItem.unit_id,
            base_price: POBCIItem.base_price,
            price: POBCIItem.price,
            added_price: POBCIItem.added_price,
            deducted_price: POBCIItem.deducted_price,
            margin: POBCIItem.margin,
            tax: POBCIItem.tax,
            tax_seller: POBCIItem.tax_seller,
            shipping_address_id: POBCIItem.shipping_address_id,
            shipping_cost: POBCIItem.shipping_cost,
            shipping_method: POBCIItem.shipping_method,
            shipping_provider: POBCIItem.shipping_provider,
            shipping_price_estimation: POBCIItem.shipping_price_estimation,
            shipping_country: POBCIItem.shipping_country,
            shipping_province: POBCIItem.shipping_province,
            shipping_city: POBCIItem.shipping_city,
            shipping_zipcode: POBCIItem.shipping_zipcode,
            shipping_address: POBCIItem.shipping_address,
            shipping_phone: POBCIItem.shipping_phone,
            shipping_phone_prefix: POBCIItem.shipping_phone_prefix,
            shipping_district: POBCIItem.shipping_district,
            shipping_receiver_name: POBCIItem.shipping_receiver_name,
            shipping_date: POBCIItem.shipping_date,
            shipping_airway_bill: POBCIItem.shipping_airway_bill,
            shipping_airway_bill_image: POBCIItem.shipping_airway_bill_image,
            shipper_name: POBCIItem.shipper_name,
            shipper_phone: POBCIItem.shipper_phone,
            billing_address_id: POBCIItem.billing_address_id,
            billing_country: POBCIItem.billing_country,
            billing_province: POBCIItem.billing_province,
            billing_city: POBCIItem.billing_city,
            billing_zipcode: POBCIItem.billing_zipcode,
            billing_address: POBCIItem.billing_address,
            billing_phone: POBCIItem.billing_phone,
            billing_phone_prefix: POBCIItem.billing_phone_prefix,
            billing_district: POBCIItem.billing_district,
            billing_receiver_name: POBCIItem.billing_receiver_name,
            billing_receiver_email: POBCIItem.billing_receiver_email,
            discount: POBCIItem.discount,
            recipient: POBCIItem.recipient,
            receipt_date: POBCIItem.receipt_date,
            delivery_order: POBCIItem.delivery_order,
            delivery_order_image: POBCIItem.delivery_order_image,
            recipient_phone_no: POBCIItem.recipient_phone_no,
            delivery_note: POBCIItem.delivery_note,
            notes: POBCIItem.notes,
            status: POBCINew.status,
            created_at: POBCIItem.created_at,
            updated_at: POBCIItem.updated_at,
            deleted_at: POBCIItem.deleted_at
        };
    });
};

exports.createPOItemVendorFromPOBCIItems = (POVendor, POItemsBCI) => {
    return POItemsBCI.map((POBCIItem) => {
        return {
            pair_id: POBCIItem.new_pair_id,
            purchase_order_id: POVendor.id,
            revision_id: POBCIItem.revision_id,
            company_product_id: POBCIItem.company_product_id,
            warehouse_id: POBCIItem.warehouse_id,
            sku_id: POBCIItem.sku_id,
            sku_no: POBCIItem.sku_no,
            qty: POBCIItem.qty,
            unit_id: POBCIItem.unit_id,
            base_price: POBCIItem.base_price,
            price: POBCIItem.price,
            added_price: POBCIItem.added_price,
            deducted_price: POBCIItem.deducted_price,
            margin: POBCIItem.margin,
            tax: POBCIItem.tax,
            tax_seller: POBCIItem.tax_seller,
            shipping_address_id: POBCIItem.shipping_address_id,
            shipping_cost: POBCIItem.shipping_cost,
            shipping_method: POBCIItem.shipping_method,
            shipping_provider: POBCIItem.shipping_provider,
            shipping_price_estimation: POBCIItem.shipping_price_estimation,
            shipping_country: POBCIItem.shipping_country,
            shipping_province: POBCIItem.shipping_province,
            shipping_city: POBCIItem.shipping_city,
            shipping_zipcode: POBCIItem.shipping_zipcode,
            shipping_address: POBCIItem.shipping_address,
            shipping_phone: POBCIItem.shipping_phone,
            shipping_phone_prefix: POBCIItem.shipping_phone_prefix,
            shipping_district: POBCIItem.shipping_district,
            shipping_receiver_name: POBCIItem.shipping_receiver_name,
            shipping_date: POBCIItem.shipping_date,
            shipping_airway_bill: POBCIItem.shipping_airway_bill,
            shipping_airway_bill_image: POBCIItem.shipping_airway_bill_image,
            shipper_name: POBCIItem.shipper_name,
            shipper_phone: POBCIItem.shipper_phone,
            billing_address_id: POBCIItem.billing_address_id,
            billing_country: POBCIItem.billing_country,
            billing_province: POBCIItem.billing_province,
            billing_city: POBCIItem.billing_city,
            billing_zipcode: POBCIItem.billing_zipcode,
            billing_address: POBCIItem.billing_address,
            billing_phone: POBCIItem.billing_phone,
            billing_phone_prefix: POBCIItem.billing_phone_prefix,
            billing_district: POBCIItem.billing_district,
            billing_receiver_name: POBCIItem.billing_receiver_name,
            billing_receiver_email: POBCIItem.billing_receiver_email,
            discount: POBCIItem.discount,
            recipient: POBCIItem.recipient,
            receipt_date: POBCIItem.receipt_date,
            delivery_order: POBCIItem.delivery_order,
            delivery_order_image: POBCIItem.delivery_order_image,
            recipient_phone_no: POBCIItem.recipient_phone_no,
            delivery_note: POBCIItem.delivery_note,
            notes: POBCIItem.notes,
            status: POVendor.status,
            created_at: POBCIItem.created_at,
            updated_at: POBCIItem.updated_at,
            deleted_at: POBCIItem.deleted_at
        };
    });
};

module.exports = exports;