const { generatePONumber } = require('../../helpers/number_generator');
const { getPOStatusFromSO } = require('../../helpers/status_mapper');

exports.model = (Sequelize, connection) => {
    const PurchaseOrder = connection.define('PurchaseOrder', {
        id: {
            type: Sequelize.BIGINT,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        revision_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        sales_order_id: {
            type: Sequelize.BIGINT,
            allowNull: true
        },
        sales_order_no: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        group_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        ref: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        channel: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        channel_type: {
            type: Sequelize.STRING(10),
            allowNull: true
        },
        payment_type_id: {
            type: Sequelize.STRING(10),
            allowNull: false
        },
        payment_method_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        company_buyer_id: {
            type: Sequelize.BIGINT,
            allowNull: false
        },
        company_buyer_name: {
            type: Sequelize.STRING(10),
            allowNull: false
        },
        company_seller_id: {
            type: Sequelize.BIGINT,
            allowNull: false
        },
        company_seller_name: {
            type: Sequelize.STRING(10),
            allowNull: false
        },
        is_partial: {
            type: Sequelize.BOOLEAN,
            allowNull: false
        },
        purchase_order_no: {
            type: Sequelize.STRING(255),
            allowNull: false
        },
        pre_purchase_order_no: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        purchase_order_no_internal: {
            type: Sequelize.STRING(255),
            allowNull: true
        },
        notes: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        discount: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        tax: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        shipping_cost: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        subtotal: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        po_amount_total: {
            type: Sequelize.DECIMAL,
            allowNull: true
        },
        po_total_sku: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        term_of_payment: {
            type: Sequelize.INTEGER(11),
            allowNull: true,
            defaultValue: '30'
        },
        status: {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true,
            defaultValue: '50'
        },
        wms_notes: {
            type: Sequelize.STRING(),
            allowNull: true
        },
        created_by: {
            type: Sequelize.BIGINT,
            allowNull: false
        },
        created_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        updated_at: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
        },
        deleted_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        expired_at: {
            type: Sequelize.DATE,
            allowNull: true
        }
    }, {
            tableName: 'purchase_order',
            freezeTableName: true,
            underscored: true,
            paranoid: true
        }
    );

    return PurchaseOrder;
};

exports.createPOBCICustomerFromPOBCICustomerOld = (POBCIOld, SOCustomerNew) => {
    return {
        revision_id: POBCIOld.revision_id,
        sales_order_id: SOCustomerNew.id,
        sales_order_no: SOCustomerNew.sales_order_no,
        group_id: POBCIOld.group_id,
        ref: POBCIOld.ref,
        channel: 4,
        channel_type: 'MP',
        payment_type_id: POBCIOld.payment_type_id,
        payment_method_id: POBCIOld.payment_method_id,
        company_buyer_id: POBCIOld.company_buyer_id,
        company_buyer_name: POBCIOld.company_buyer_name,
        company_seller_id: process.env.BCI_ID,
        company_seller_name: process.env.BCI_NAME,
        is_partial: POBCIOld.is_partial,
        purchase_order_no: POBCIOld.purchase_order_no,
        pre_purchase_order_no: POBCIOld.pre_purchase_order_no,
        purchase_order_no_internal:POBCIOld.purchase_order_no_internal,
        notes: POBCIOld.notes,
        discount: POBCIOld.discount,
        tax: POBCIOld.tax,
        shipping_cost: POBCIOld.shipping_cost,
        subtotal: POBCIOld.subtotal,
        po_amount_total: POBCIOld.po_amount_total,
        po_total_sku: POBCIOld.po_total_sku,
        term_of_payment: POBCIOld.term_of_payment,
        status: getPOStatusFromSO(SOCustomerNew.status),
        wms_notes: POBCIOld.delivery_notes,
        created_by: POBCIOld.created_by,
        created_at: POBCIOld.created_at,
        updated_at: POBCIOld.updated_at,
        deleted_at: POBCIOld.deleted_at,
        expired_at: POBCIOld.expired_at
    };
};

exports.createPOVendorFromPOBCICustomerOld = (POBCIOld, SOBCI) => {
    console.log(POBCIOld.purchase_order_no);
    return {
        revision_id: POBCIOld.revision_id,
        sales_order_id: SOBCI.id,
        sales_order_no: SOBCI.sales_order_no,
        group_id: POBCIOld.group_id,
        ref: POBCIOld.ref,
        channel: 4,
        channel_type: 'MP',
        payment_type_id: POBCIOld.payment_type_id,
        payment_method_id: POBCIOld.payment_method_id,
        company_buyer_id: process.env.BCI_ID,
        company_buyer_name: process.env.BCI_NAME,
        company_seller_id: POBCIOld.company_seller_id,
        company_seller_name: POBCIOld.company_seller_name,
        is_partial: POBCIOld.is_partial,
        purchase_order_no: POBCIOld.purchase_order_no,
        pre_purchase_order_no: POBCIOld.pre_purchase_order_no,
        purchase_order_no_internal:POBCIOld.purchase_order_no,
        notes: POBCIOld.notes,
        discount: POBCIOld.discount,
        tax: POBCIOld.tax,
        shipping_cost: POBCIOld.shipping_cost,
        subtotal: POBCIOld.subtotal,
        po_amount_total: POBCIOld.po_amount_total,
        po_total_sku: POBCIOld.po_total_sku,
        term_of_payment: POBCIOld.term_of_payment,
        status: POBCIOld.status,
        wms_notes: POBCIOld.delivery_notes,
        created_by: POBCIOld.created_by,
        created_at: POBCIOld.created_at,
        updated_at: POBCIOld.updated_at,
        deleted_at: POBCIOld.deleted_at,
        expired_at: POBCIOld.expired_at
    };
};
module.exports = exports;